﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace assessment2
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();

            var Inform = new List<Movie>();

            a.Source = Inform[0].Url;
            b.Source = Inform[1].Url;
            c.Source = Inform[2].Url;
            d.Source = Inform[3].Url;
            e.Source = Inform[4].Url;
            f.Source = Inform[5].Url;
            g.Source = Inform[6].Url;
            h.Source = Inform[7].Url;
            i.Source = Inform[8].Url;
            j.Source = Inform[9].Url;
            k.Source = Inform[10].Url;
            l.Source = Inform[11].Url;
            m.Source = Inform[12].Url;
            n.Source = Inform[13].Url;
            o.Source = Inform[14].Url;
            p.Source = Inform[15].Url;
            q.Source = Inform[16].Url;
            r.Source = Inform[17].Url;
            s.Source = Inform[18].Url;
            t.Source = Inform[19].Url;

            var tap = new TapGestureRecognizer();
            tap.Tapped += Tapped;

            a.GestureRecognizers.Add(tap);
            b.GestureRecognizers.Add(tap);
            c.GestureRecognizers.Add(tap);
            d.GestureRecognizers.Add(tap);
            e.GestureRecognizers.Add(tap);
            f.GestureRecognizers.Add(tap);
            g.GestureRecognizers.Add(tap);
            h.GestureRecognizers.Add(tap);
            i.GestureRecognizers.Add(tap);
            j.GestureRecognizers.Add(tap);
            k.GestureRecognizers.Add(tap);
            l.GestureRecognizers.Add(tap);
            m.GestureRecognizers.Add(tap);
            n.GestureRecognizers.Add(tap);
            o.GestureRecognizers.Add(tap);
            p.GestureRecognizers.Add(tap);
            q.GestureRecognizers.Add(tap);
            r.GestureRecognizers.Add(tap);
            s.GestureRecognizers.Add(tap);
            t.GestureRecognizers.Add(tap);
        }

         List<Movie> Inform = new List<Movie>
        {
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SY1000_CR0,0,672,1000_AL_.jpg", Desc = "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.", Rating = "5/5", Name = "The Shawshank Redemtion", Genre = "Drama",  },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BZTRmNjQ1ZDYtNDgzMy00OGE0LWE4N2YtNTkzNWQ5ZDhlNGJmL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,704,1000_AL_.jpg", Desc = "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.", Rating = "4.5/5", Name = "The God Father", Genre = "Drama" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,702,1000_AL_.jpg", Desc = "The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on the family crime syndicate.", Rating = "3/5", Name = "The God Father II", Genre = "Drama" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg", Desc = "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the Dark Knight must come to terms with one of the greatest psychological tests of his ability to fight injustice.", Rating = "3/5", Name = "The Dark Knight", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BNTE2Nzg1NjkzNV5BMl5BanBnXkFtZTgwOTgyODMyMTI@._V1_SY1000_CR0,0,631,1000_AL_.jpg", Desc = "A young African-American man visits his Caucasian girlfriend's mysterious family estate.", Rating = "5/5", Name = "Get Out", Genre = "Comedy" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BNjRhMGI3ZGItZTMzMS00NzdmLWI1MzMtNjk0ZmY5ZjMyZDdkL2ltYWdlXkEyXkFqcGdeQXVyNTQ3MjE4NTU@._V1_SY1000_CR0,0,674,1000_AL_.jpg", Desc = "An American backpacker gets involved with a ring of drug smugglers as their driver, though he winds up on the run from his employers across Cologne high-speed Autobahn.", Rating = "2/5", Name = "Collide", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjA5MTAwMzM3OF5BMl5BanBnXkFtZTgwNTk4NjYyMTI@._V1_SY1000_SX648_AL_.jpg", Desc = "When a radio falls from the sky into the hands of a wide-eyed Tibetan Mastiff, he leaves home to fulfill his dream of becoming a musician, setting into motion a series of completely unexpected events.", Rating = "3/5", Name = "Rock Dog", Genre = "Family" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkwMTczNzUxNV5BMl5BanBnXkFtZTgwNzA4Mjc2MDI@._V1_SY1000_SX675_AL_.jpg", Desc = "Set in 1930s Ukraine, as Stalin advances the ambitions of communists in the Kremlin, young artist Yuri battles to save his lover Natalka from the Holodomor, the death-by-starvation program that ultimately killed millions of Ukrainians.", Rating = "4/5", Name = "Bitter Harvest", Genre = "Romance" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BZWFiMjdjODktMWY4YS00MWU0LThiNDUtNDI4ODY0NzhhMGFkXkEyXkFqcGdeQXVyNjU1MjQzMjQ@._V1_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "3.5/5", Name = "As You Are", Genre = "Family" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODMwM2M4ZmItMzk4Ny00YTBhLTkyN2EtYTAzMmIwNjMxNjQxL2ltYWdlXkEyXkFqcGdeQXVyMTk0NTY2ODQ@._V1_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "1.7/5", Name = "Punching Henry", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTcyNTEyOTY0M15BMl5BanBnXkFtZTgwOTAyNzU3MDI@._V1_SY1000_CR0,0,674,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "4/5", Name = "Lego Batman Movie", Genre = "Family"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjE2NDkxNTY2M15BMl5BanBnXkFtZTgwMDc2NzE0MTI@._V1_SY1000_CR0,0,648,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "3/5", Name = "John Wick II", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODQwOTc5MDM2N15BMl5BanBnXkFtZTcwODQxNTEzNA@@._V1_SY1000_CR0,0,666,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "2/5", Name = "12 Angry Men", Genre = "Thriller" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,666,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "", Name = "Schidler's List", Genre = "Drama" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SY1000_CR0,0,673,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "5/5", Name = "Pulp Fiction", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYWY1ZWQ5YjMtMDE0MS00NWIzLWE1M2YtODYzYTk2OTNlYWZmXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SY1000_SX668_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "", Name = "Lord of the Rings", Genre = "Action" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_SY1000_CR0,0,656,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "4/5", Name = "The Good the Bad & the Ugly", Genre = "Western" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BZGY5Y2RjMmItNDg5Yy00NjUwLThjMTEtNDc2OGUzNTBiYmM1XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "5/5", Name = "FightClub", Genre = "Drama" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYmViY2M2MTYtY2MzOS00YjQ1LWIzYmEtOTBiNjhlMGM0NjZjXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY1000_CR0,0,644,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "5/5", Name = "Star Wars", Genre = "Fantasy" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYThjM2MwZGMtMzg3Ny00NGRkLWE4M2EtYTBiNWMzOTY0YTI4XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY1000_CR0,0,757,1000_AL_.jpg", Desc = "Set in the early 1990's, As You Are is the telling and retelling of a relationship between three teenagers as it traces the course of their friendship through a construction of disparate memories prompted by a police investigation.", Rating = "5/5", Name = "Forest Gump", Genre = "Drama" },
        };


        public void Tapped(object sender, EventArgs e)
            {

                var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 4;

                var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);



                Navigation.PushAsync(new DetailPage(Inform[a + b].Url, Inform[a + b].Desc, Inform[a + b].Name, Inform[a + b].Rating));


            }
        }
    }

